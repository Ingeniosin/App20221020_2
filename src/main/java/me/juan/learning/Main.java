package me.juan.learning;

import me.juan.learning.manager.MasterManager;
import me.juan.learning.manager.PokemonManager;
import me.juan.learning.models.Master;
import me.juan.learning.models.Pokemon;
import me.juan.learning.models.variant.PokemonVariants;

public class Main {

    public static void main(String[] args) {
        PokemonManager pokemonManager = PokemonManager.getInstance();
        MasterManager masterManager = MasterManager.getInstance();

        Master juan = masterManager.findOrCreate("Juan");
        Master pedro = masterManager.findOrCreate("Pedro");
        Master juan2 = masterManager.findOrCreate("Juan");

        System.out.println(juan.getPokemonDescription("Pikachu", PokemonVariants.ELECTRIC.getName()));
        System.out.println(pedro.getPokemonDescription("Charmander", PokemonVariants.FIRE.getName()));
        System.out.println(juan2.getPokemonDescription("Bulbasaur", PokemonVariants.GRASS.getName()));

        Pokemon electricPikachu = pokemonManager.findOrCreate("Pikachu", PokemonVariants.ELECTRIC.getName());

        System.out.println("");
        System.out.println("\t ----- CLONE <- Prototype Pattern ------");

        Pokemon clonePicachu = pokemonManager.clone(electricPikachu);
        clonePicachu.setVariant(PokemonVariants.FIRE.getName());

        System.out.println("\t\t* "+clonePicachu.getDescription());


    }

}

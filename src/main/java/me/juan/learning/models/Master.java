package me.juan.learning.models;

public interface Master {
    String getName();

    String getPokemonDescription(String name, String variant);

}

package me.juan.learning.models.pokemon;

import lombok.Setter;
import lombok.experimental.Accessors;
import me.juan.learning.models.Pokemon;

@Setter
@Accessors(chain = true)
public class PokemonBuilder {

    private String name;
    private String variant;

    public Pokemon build() {
        return new GenericPokemon(name, variant);
    }
}

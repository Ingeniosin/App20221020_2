package me.juan.learning.models.pokemon;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import me.juan.learning.models.Pokemon;

@Data
@ToString
public class GenericPokemon implements Pokemon {

    private final String name;
    private String variant;

    public GenericPokemon(String name, String variant) {
        this.name = name;
        this.variant = variant;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return "Un pokemon llamado "+name+" de tipo " + variant;
    }

    @Override
    public String getVariant() {
        return variant;
    }

    @Override
    public Pokemon clone() {
        try {
            return (Pokemon) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

}

package me.juan.learning.manager;

import me.juan.learning.models.Master;
import me.juan.learning.models.master.GenericMaster;

import java.util.HashMap;
import java.util.Map;

public class MasterManager {

    private static MasterManager instance;

    public static MasterManager getInstance() {
        if(instance == null)
            instance = new MasterManager();
        return instance;
    }

    private final Map<String, Master> masters = new HashMap<>();


    public Master findOrCreate(String name) {
        String key = name.toLowerCase().trim();
        return masters.computeIfAbsent(key, GenericMaster::new);
    }

}

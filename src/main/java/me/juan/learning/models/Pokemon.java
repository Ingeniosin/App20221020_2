package me.juan.learning.models;

public interface Pokemon extends Cloneable {

    String getName();

    String getDescription();

    String getVariant();
    void setVariant(String variant);

    Pokemon clone();

}

package me.juan.learning.models.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import me.juan.learning.manager.PokemonManager;
import me.juan.learning.models.Master;
import me.juan.learning.models.Pokemon;

@Data
@AllArgsConstructor
@ToString
public class GenericMaster  implements Master {

    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getPokemonDescription(String name, String variant) {
        Pokemon pokemon = PokemonManager.getInstance().findOrCreate(name, variant);
        return "\t* " + name + " encontro un pokemon llamado "+pokemon.getName()+" de tipo  " + pokemon.getVariant()+"\n\t\t -> "+ pokemon.getDescription();
    }
}

package me.juan.learning.models.variant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PokemonVariants {

    ELECTRIC("Electrico"),
    WATER("Agua"),
    GRASS("Hierva"),
    FIRE("FUEGO");


    private final String name;

}

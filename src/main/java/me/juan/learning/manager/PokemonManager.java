package me.juan.learning.manager;

import me.juan.learning.models.Pokemon;
import me.juan.learning.models.pokemon.PokemonBuilder;

import java.util.HashMap;
import java.util.Map;

public class PokemonManager {
    private static PokemonManager instance;

    public static PokemonManager getInstance() {
        if(instance == null)
            instance = new PokemonManager();
        return instance;
    }

    private final Map<String, Pokemon> pokemons = new HashMap<>();

    public Pokemon findOrCreate(String name, String variant) {
        String key = name + " - " + variant;
        return pokemons.computeIfAbsent(key.toLowerCase().trim(), x -> new PokemonBuilder().setName(name).setVariant(variant).build());
    }

    public Pokemon clone(Pokemon pokemon) {
        return pokemon.clone();
    }
}
